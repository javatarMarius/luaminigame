--
-- Created by IntelliJ IDEA.
-- User: Пользователь
-- Date: 27.09.13
-- Time: 14:59
-- To change this template use File | Settings | File Templates.
--


--создадим "класс" автомобиль
class_car = {}
--инициализируем поля класса
function class_car:new(model, color)
    local object = {}
    object.model = model or "UAZ" -- по умолчанию это будет УАЗ
    object.color = color or "ponos" -- поносового цвета
    --далее идет превращение таблицы в "класс", объяснять не буду, т.к для этого нужна отдельная статья
    setmetatable(object,self)
    self.__index = self --перед index стоит двойное подчеркивание!
    return object -- возвращаем объект!
end

--создадим первый автомобиль!
my_auto = class_car:new("BMW",nil) --получим BMW поносового цвета XD

--функция изменения
function class_car:set(model, color)
    self.model = model
    self.color = color
end

--функция получения результата
function class_car:get()
    return self.model, self.color
end

-- меняем себе машину ))
my_auto:set("AUDI", "BLACK")
-- проверяем
print(my_auto:get()) -- получим AUDI

class_moto = {} -- инициализируем класс мото
--пусть у него будет один свой метод
function class_moto:ololo()
    print("trololo")
end
--переназначение метода
function class_moto:get()
    return "2000", "year"
end
--наследуемся
setmetatable(class_moto,{__index = class_car})

my_moto = class_moto:new() --создали мото порожденный от класса class_car

--меняем название и цвет
my_moto:set("URAL","BLUE")
print(my_moto:get()) -->мы увидим 2000 year
print(class_car.get(my_moto)) --> URAL BLUE