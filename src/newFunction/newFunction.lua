--1. Функция, добавляющая listener объекту,
-- -- который (listener) удаляется сразу после первого вызова.

function addSelfRemovingEventListener(object, eventName, handler)
    local function localHandler(e)
        object:removeEventListener(eventName, localHandler)
        handler(e)
    end

    object:addEventListener(eventName, localHandler)
end

-- Пример:
addSelfRemovingEventListener(box, 'touch', boxDestroy)

--=================================================================================

--4. Функция, перемещающая объект из одной
-- -- группы в другую, сохраняя его позицию на экране.

function changeGroup(object, group)
    local x, y = localToLocal(object, 0, 0, group)
    group:insert(object)
    object.xOrigin, object.yOrigin = x, y
end

-- Пример:
changeGroup(car, roadGroup)

--=================================================================================

-- Функция, проверяющая наличие файла в
-- -- директории. Возвращает true при положительном исходе.

function fileExists(file, dir)
    local path = system.pathForFile(file, dir or system.DocumentsDirectory)
    if not path then return false end
    local handle = io.open(path, 'r')
    if handle then
        handle:close()
        return true
    else
        return false
    end
end

-- Пример:
if fileExists('1.mp3', 'sounds/profile2') then
end

--=================================================================================

--Функция, масштабирующая экранный объект
-- -- под размеры заданной прямоугольной области,
-- -- если он больше ее по одному из значений
-- -- высота/ширина. Масштабирует и меньшие изображения
-- -- если параметр alwaysScale выставлен в true.

function fitObjectInArea(obj, width, height, alwaysScale)
    local objW, objH = obj.width, obj.height
    if alwaysScale or objW > width or objH > height then
        local scale = math.min(width / objW, height / objH)
        obj.xScale, obj.yScale = scale, scale
    end
    return obj
end

--П р и м е р :
car = fitObjectInArea(car, 300, 150, false)

--=================================================================================

--Функция, подгоняющая текст под заданную ширину, сохраняя его пропорции.

function fitTextInArea(txtObj, maxWidth)
    while txtObj.contentWidth > maxWidth do
        txtObj.size = txtObj.size - 2
    end
    return txtObj
end

--П р и м е р :
winText = fitTextInArea(winText, display.viewableContentWidth * 0.5)
