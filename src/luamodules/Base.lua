local base = _G;

module('Base')

mtab = { __index = _M }

local Factory = base.require('Factory')

function new()
    return Factory.create(_M)
end

function construct(self)
    base.print('Base class created')
    self.field = 'text'
end

function setField(self, field)
    self.field = field
end

function getField(self)
    return self.field
end


