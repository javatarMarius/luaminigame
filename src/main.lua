--package.path = './luamodules/?.lua' -- пути к Lua библиотекам
--package.cpath = './cmodules/?.dll' -- пути к С библиотекам
--
--require('Base')
--require('Child')
--
--local base = Base.new()
--print(base:getField())
--base:setField(1)
--print(base:getField())

--====================================
--           Hide Status Bar
--====================================

display.setStatusBar(display.HiddenStatusBar)

--====================================
--           Physics Engine
--====================================

local physics = require'physics'
physics.start()
physics.setGravity(0, 0)

--====================================
--           Constants
--====================================

local clickButton = "tap"

local idStartButton = "startButton"
local idAboutButton = "aboutButton"
local idPauseButton = "pauseButton"
local idResumeGame = "resumeGame"
local idRestartGame = "restartGame"
local idBackToMainMenu = "backToMainMenu"

--       Images/Sound Resources

local imagBtnAbout = "gfx/aboutB.png"
local imgGameScreenPath = "gfx/bg.png"
local bgMusicPath = "sound/bgMusic.mp3"
local imagBtnStartGame = "gfx/startB.png"
local imgPauseButtonPath = "gfx/pause.png"
local bgMenuScreenPath = "gfx/mScreen.png"
local imgResumeGamePath = "gfx/resume.png"
local imgRestartGamePath = "gfx/restart.png"
local imgAboutScreenPath = "gfx/aboutScreen.png"
local imgBackToMainMenuGamePath = "gfx/backToMainMenu.png"

--====================================
--            MenuScreen
--====================================

local aboutBtn
local startBtn
local menuScreen
local loadMenuMusic
local playMenuMusic
local menuScreenGroup
local pauseScreenGroup

--====================================
--            About Screen
--====================================

local aboutScreen

--====================================
--            Game Screen
--===================================
local gameScreen
local levelScore

--====================================
--            Pause Screen
--====================================

local pauseScreen
local pauseButton
local pauseDarkRect
local resumeGameButton
local restartGameButton
local backToMainMenuButton

--====================================
--      Text (Score/Level/About)
--====================================

local scoreText
local levelText

--====================================
--          Functions
--====================================

local update = {}
local createMenuScene = {}
local createPauseMenu = {}
local removePauseMenu = {}
local createGameScene = {}
local hideAboutScreen = {}
local createAboutScene = {}
local removeAboutScene = {}
local loadSoundResources = {}
local tweemBetweenScreens = {}
local createrPauseMenu = {}
local pauseMenuButtonPressed = {}

--====================================
--          Main
--====================================

local function Main()
    createMenuScene()
    loadSoundResources()
end

--======================================================================
--                  work with MAIN MENU scene
--======================================================================

function createMenuScene()
    aboutBtn = display.newImage(imagBtnAbout)
    startBtn = display.newImage(imagBtnStartGame)
    menuScreen = display.newImage(bgMenuScreenPath)

    startBtn.name = idStartButton
    aboutBtn.name = idAboutButton

    startBtn.x = 160
    startBtn.y = 240
    aboutBtn.x = 160
    aboutBtn.y = 330

    menuScreenGroup = display.newGroup()
    menuScreenGroup:insert(menuScreen)
    menuScreenGroup:insert(startBtn)
    menuScreenGroup:insert(aboutBtn)

    startBtn:addEventListener(clickButton, tweemBetweenScreens)
    aboutBtn:addEventListener(clickButton, tweemBetweenScreens)
    --    print("create menu scene")
end

--======================================================================
--                     load resources
--======================================================================

function loadSoundResources()
    loadMenuMusic = audio.loadStream(bgMusicPath)
end

--======================================================================
--      tweem between scence MAIN_MENU / ABOUT_GAME / GAME
--======================================================================

function tweemBetweenScreens:tap(e)
    if (e.target.name == idStartButton) then
        createGameScene()
    elseif (e.target.name == idAboutButton) then
        createAboutScene()
    elseif (e.target.name == idPauseButton) then
        createrPauseMenu()
        print("PRESS IN PAUSE BUTTON IN GAME_SCENE")
    end
end

--======================================================================
--                  Work with ABOUT Scene
--======================================================================

function createAboutScene()
    aboutScreen = display.newImage(imgAboutScreenPath)
    transition.from(aboutScreen,
        {
            time = 300,
            x = menuScreen.contentWidth,
            transition = easing.outExpo
        })
    startBtn.isVisible = false;
    aboutBtn.isVisible = false;
    aboutScreen:addEventListener(clickButton, hideAboutScreen)
    print("CREATE ABOUT SCENE")
end

function hideAboutScreen()
    transition.to(aboutScreen,
        {
            time = 300,
            x = aboutScreen.width * 1.5,
            transition = easing.outExpo,
            onComplete = removeAboutScene
        })
end

function removeAboutScene()
    startBtn.isVisible = true;
    aboutBtn.isVisible = true;
    aboutScreen:removeSelf()
    aboutScreen = nil
    print("REMOVE ABOUT_SCENE")
end

--======================================================================
--                      Work with GAME Scene
--======================================================================

function createGameScene()
    menuScreenGroup:removeSelf()
    menuScreenGroup = nil
    gameScreen = display.newImage(imgGameScreenPath)
    pauseButton = display.newImage(imgPauseButtonPath)
    pauseButton.name = idPauseButton
    pauseButton:addEventListener(clickButton, tweemBetweenScreens)
    print("CREATE AND  ENTER IN GAME SCENE")
end

--======================================================================
--                    work with PAUSE Scene
--======================================================================


function createrPauseMenu()
    setImageForPauseMenu()
    setIdForPauseMenuButtons()
    setCoordinateForPauseMenuButtons()
    setEventListenerForPauseMenuButton()
    setMembersPauseMenuGroups()
end

function setImageForPauseMenu()
    pauseDarkRect = display.newRect(25, 30, 320 - 50, 480 - 50)
    pauseDarkRect:setFillColor(140, 140, 140)
    pauseButton.isVisible = false
    resumeGameButton = display.newImage(imgResumeGamePath)
    restartGameButton = display.newImage(imgRestartGamePath)
    backToMainMenuButton = display.newImage(imgBackToMainMenuGamePath)
end

function setIdForPauseMenuButtons()
    resumeGameButton.name = idResumeGame
    restartGameButton.name = idRestartGame
    backToMainMenuButton.name = idBackToMainMenu
end

function setCoordinateForPauseMenuButtons()
    resumeGameButton.x = pauseDarkRect.x
    resumeGameButton.y = pauseDarkRect.y - 130

    restartGameButton.x = pauseDarkRect.x
    restartGameButton.y = pauseDarkRect.y - 130 + 100

    backToMainMenuButton.x = pauseDarkRect.x
    backToMainMenuButton.y = pauseDarkRect.y - 130 + 200
end

function setEventListenerForPauseMenuButton()
    resumeGameButton:addEventListener(clickButton, pauseMenuButtonPressed)
    restartGameButton:addEventListener(clickButton, pauseMenuButtonPressed)
    backToMainMenuButton:addEventListener(clickButton, pauseMenuButtonPressed)
end

function setMembersPauseMenuGroups()
    pauseScreenGroup = display.newGroup()
    pauseScreenGroup:insert(pauseDarkRect)
    pauseScreenGroup:insert(resumeGameButton)
    pauseScreenGroup:insert(restartGameButton)
    pauseScreenGroup:insert(backToMainMenuButton)
end

function resumeGame()
    removePauseScreenGroup()
    pauseButton.isVisible = true
    print("---- RESUME --- ")
end

local function doYouWantRestartGame(event)

    local action = event.action

    if "clicked" == event.action then
        if 2 == event.index then
            removePauseScreenGroup()
            print("---  RESTART --- ")
        end

    elseif "cancelled" == event.action then
        -- our cancelAlert timer function dismissed the alert so do nothing
    end
end

function restartGame()
    local alert = native.showAlert("Restart Game?", -- title
        "Do You want restart game ?",
        { "NO", "OK" },
        doYouWantRestartGame)
    native.cancelAlert(alert)
end

function backToMainMenu()

    removePauseScreenGroup()
    gameScreen:removeSelf()
    pauseButton:removeSelf()
    gameScreen = nil
    pauseButton = nil
    createMenuScene()
    print("---  BACK TO MAIN MENU --- ")
end

function removePauseScreenGroup()
    pauseScreenGroup:removeSelf()
    pauseScreenGroup = nil
    print("DESTROY ALL OBJECT IN PAUSE MENU")
end

--======================================================================
--     press all buttons in PAUSE_SCENE
--======================================================================

function pauseMenuButtonPressed:tap(e)
    if (e.target.name == idResumeGame) then
        resumeGame()
    elseif (e.target.name == idRestartGame) then
        restartGame()
    elseif (e.target.name == idBackToMainMenu) then
        backToMainMenu()
    end
end

Main()



