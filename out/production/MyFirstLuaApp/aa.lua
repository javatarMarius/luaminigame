local widget = require("widget")
--*******************************************************************

display.setStatusBar(display.HiddenStatusBar);
-- грузим задний фон
local background = display.newImage("world.jpg");
background:rotate(90);

--local musicGame = audio.loadSound("action_music.mp3");
local buzzAudio = audio.loadStream("action_music.mp3")
audio.play(buzzAudio)

--***********************************************************************************************************
-- создаем текст системным шрифтом и размером 50
local helloText = display.newText("First App", 0, 0, native.systemFont, 50);
helloText.x = display.contentWidth / 2;
helloText.y = display.contentHeight / 2;
helloText:setTextColor(100, 21, 35);

--***********************************************************************************************************

local catImage = graphics.newImageSheet("runningcat.png", { width = 512, height = 256, numFrames = 8 })
local catSprite = display.newSprite(catImage, { name = "cat", start = 1, count = 8, time = 1000 })
catSprite.x = 150;
catSprite.y = display.contentHeight - 256;
catSprite.xScale = .5;
catSprite.yScale = .5;
catSprite:play();

--***********************************************************************************************************

local manImage = graphics.newImageSheet("greenman.png", { width = 128, height = 128, numFrames = 15 });
local manSprite = display.newSprite(manImage, { name = "man", start = 1, count = 15, time = 1800 });
manSprite.x = 240;
manSprite.y = 320;
manSprite.xScale = .5;
manSprite.yScale = .5;
manSprite:play();

--***********************************************************************************************************

local pressPlayMusicButton = function(event)

end

local pressStopMusicButton = function(event)
end

local stopMusic = widget.newButton{
    defaultFile = "buttonRed.png",
    overFile = "buttonRedOver.png",
    label = "Stop Music Button",
    emboss = true,
    onPress = pressPlayMusicButton,
    onRelease = pressStopMusicButton
}

stopMusic.x = 160; stopMusic.y = 160



